package main

import (
	"gitlab.com/shapeblock-buildpacks/nginx-cnb/nginx"

	"github.com/paketo-buildpacks/packit"
)

func main() {
	packit.Detect(nginx.Detect())
}
