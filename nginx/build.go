package nginx

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"text/template"

	"github.com/paketo-buildpacks/packit"
	"gopkg.in/yaml.v2"
)

type NginxConfig struct {
	WebDirectory string
	AppRoot      string
	FpmSocket    string
}

type SupervisorConfig struct {
	FpmCmd   string
	NginxCmd string
}

const NginxConfTemplate = `daemon     off;
error_log  stderr  notice;
worker_processes  auto;
events {
    worker_connections  1024;
}
http {
    types {
        text/html                             html htm shtml;
        text/css                              css;
        text/xml                              xml;
        image/gif                             gif;
        image/jpeg                            jpeg jpg;
        application/javascript                js;
        application/atom+xml                  atom;
        application/rss+xml                   rss;
        text/mathml                           mml;
        text/plain                            txt;
        text/vnd.sun.j2me.app-descriptor      jad;
        text/vnd.wap.wml                      wml;
        text/x-component                      htc;
        image/png                             png;
        image/tiff                            tif tiff;
        image/vnd.wap.wbmp                    wbmp;
        image/x-icon                          ico;
        image/x-jng                           jng;
        image/x-ms-bmp                        bmp;
        image/svg+xml                         svg svgz;
        image/webp                            webp;
        application/font-woff                 woff;
        application/java-archive              jar war ear;
        application/json                      json;
        application/mac-binhex40              hqx;
        application/msword                    doc;
        application/pdf                       pdf;
        application/postscript                ps eps ai;
        application/rtf                       rtf;
        application/vnd.ms-excel              xls;
        application/vnd.ms-fontobject         eot;
        application/vnd.ms-powerpoint         ppt;
        application/vnd.wap.wmlc              wmlc;
        application/vnd.google-earth.kml+xml  kml;
        application/vnd.google-earth.kmz      kmz;
        application/x-7z-compressed           7z;
        application/x-cocoa                   cco;
        application/x-java-archive-diff       jardiff;
        application/x-java-jnlp-file          jnlp;
        application/x-makeself                run;
        application/x-perl                    pl pm;
        application/x-pilot                   prc pdb;
        application/x-rar-compressed          rar;
        application/x-redhat-package-manager  rpm;
        application/x-sea                     sea;
        application/x-shockwave-flash         swf;
        application/x-stuffit                 sit;
        application/x-tcl                     tcl tk;
        application/x-x509-ca-cert            der pem crt;
        application/x-xpinstall               xpi;
        application/xhtml+xml                 xhtml;
        application/zip                       zip;
        application/octet-stream              bin exe dll;
        application/octet-stream              deb;
        application/octet-stream              dmg;
        application/octet-stream              iso img;
        application/octet-stream              msi msp msm;
        application/vnd.openxmlformats-officedocument.wordprocessingml.document    docx;
        application/vnd.openxmlformats-officedocument.spreadsheetml.sheet          xlsx;
        application/vnd.openxmlformats-officedocument.presentationml.presentation  pptx;
        audio/midi                            mid midi kar;
        audio/mpeg                            mp3;
        audio/ogg                             ogg;
        audio/x-m4a                           m4a;
        audio/x-realaudio                     ra;
        video/3gpp                            3gpp 3gp;
        video/mp4                             mp4;
        video/mpeg                            mpeg mpg;
        video/quicktime                       mov;
        video/webm                            webm;
        video/x-flv                           flv;
        video/x-m4v                           m4v;
        video/x-mng                           mng;
        video/x-ms-asf                        asx asf;
        video/x-ms-wmv                        wmv;
        video/x-msvideo                       avi;
    }
    default_type       application/octet-stream;
    sendfile           on;
    keepalive_timeout  65;
    gzip               on;
    port_in_redirect   off;
    root               {{.AppRoot}}/{{.WebDirectory}};
    index              index.php index.html;
    server_tokens      off;
    log_format common '$remote_addr - $remote_user [$time_local] "$request" $status $body_bytes_sent';
    log_format extended '$remote_addr - $remote_user [$time_local] "$request" $status $body_bytes_sent vcap_request_id=$http_x_vcap_request_id';
    access_log  /dev/stdout  extended;
    # set $https only when SSL is actually used.
    map $http_x_forwarded_proto $proxy_https {
        https on;
    }
    # setup the scheme to use on redirects
    map $http_x_forwarded_proto $redirect_scheme {
        default http;
        http http;
        https https;
    }
    # map conditions for redirect
    map $http_x_forwarded_proto $redirect_to_https {
        default no;
        http yes;
        https  no;
    }
    upstream php_fpm {
        server unix:{{.FpmSocket}};
    }
    server {
        listen       8080  default_server;
        server_name localhost;
        fastcgi_temp_path      /tmp/nginx_fastcgi 1 2;
        client_body_temp_path  /tmp/nginx_client_body 1 2;
        proxy_temp_path        /tmp/nginx_proxy 1 2;
        real_ip_header         x-forwarded-for;
        set_real_ip_from       10.0.0.0/8;
        real_ip_recursive      on;
        # forward http to https
        if ($redirect_to_https = "yes") {
            return 301 https://$http_host$request_uri;
        }
        # Deny hidden files (.htaccess, .htpasswd, .DS_Store).
        location ~ /\. {
            deny            all;
            access_log      off;
            log_not_found   off;
        }
        # Some basic cache-control for static files to be sent to the browser
        location ~* \.(?:ico|css|js|gif|jpeg|jpg|png)$ {
            expires         max;
            add_header      Pragma public;
            add_header      Cache-Control "public, must-revalidate, proxy-revalidate";
        }
        location ~* \.php$ {
            try_files $uri =404;
            fastcgi_param  QUERY_STRING       $query_string;
            fastcgi_param  REQUEST_METHOD     $request_method;
            fastcgi_param  CONTENT_TYPE       $content_type;
            fastcgi_param  CONTENT_LENGTH     $content_length;
            fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
            fastcgi_param  REQUEST_URI        $request_uri;
            fastcgi_param  DOCUMENT_URI       $document_uri;
            fastcgi_param  DOCUMENT_ROOT      $document_root;
            fastcgi_param  SERVER_PROTOCOL    $server_protocol;
            fastcgi_param  HTTPS              $proxy_https if_not_empty;
            fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
            fastcgi_param  SERVER_SOFTWARE    nginx/$nginx_version;
            fastcgi_param  REMOTE_ADDR        $remote_addr;
            fastcgi_param  REMOTE_PORT        $remote_port;
            fastcgi_param  SERVER_ADDR        $server_addr;
            fastcgi_param  SERVER_PORT        $server_port;
            fastcgi_param  SERVER_NAME        $host;
            fastcgi_param HTTP_PROXY "";
            fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_pass    php_fpm;
        }
        include {{.AppRoot}}/.nginx.conf.d/*-server.conf;
    }
    include {{.AppRoot}}/.nginx.conf.d/*-http.conf;
}`

const SupervisorConfTemplate = `[program:phpfpm]
command={{.FpmCmd}}
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0

[program:nginx]
command={{.NginxCmd}}
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
`

func WriteFileFromReader(filename string, perm os.FileMode, source io.Reader) error {
	if err := os.MkdirAll(filepath.Dir(filename), 0755); err != nil {
		return err
	}

	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, perm)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = io.Copy(f, source)
	if err != nil {
		return err
	}

	return nil
}

func ProcessTemplateToFile(templateBody string, outputPath string, data interface{}) error {
	template, err := template.New(filepath.Base(outputPath)).Parse(templateBody)
	if err != nil {
		return err
	}

	var b bytes.Buffer
	err = template.Execute(&b, data)
	if err != nil {
		return err
	}

	return WriteFileFromReader(outputPath, 0644, &b)
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func Build() packit.BuildFunc {
	return func(context packit.BuildContext) (packit.BuildResult, error) {

		// fix caching
		nginxLayer, err := context.Layers.Get("nginx")
		if err != nil {
			return packit.BuildResult{}, err
		}

		nginxBin := filepath.Join(nginxLayer.Path, "sbin", "nginx")
		if !fileExists(nginxBin) {
			fmt.Printf("Installing nginx\n")

			nginxLayer, err = nginxLayer.Reset()
			if err != nil {
				return packit.BuildResult{}, err
			}

			uri := "https://buildpacks.cloudfoundry.org/dependencies/nginx/nginx_1.18.0_linux_x64_cflinuxfs3_10435f5f.tgz"
			downloadDir, err := ioutil.TempDir("", "downloadDir")
			if err != nil {
				return packit.BuildResult{}, err
			}
			defer os.RemoveAll(downloadDir)

			err = exec.Command("curl",
				uri,
				"-o", filepath.Join(downloadDir, "nginx.tar.xz"),
			).Run()
			if err != nil {
				return packit.BuildResult{}, err
			}

			cmd := exec.Command("tar",
				"-xf",
				filepath.Join(downloadDir, "nginx.tar.xz"),
				"--strip-components=1",
				"-C", nginxLayer.Path,
			)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			if err = cmd.Run(); err != nil {
				return packit.BuildResult{}, err
			}

			fmt.Printf("Installing supervisor\n")
			// Install supervisord as well.
			uri = "https://github.com/ochinchina/supervisord/releases/download/v0.7.3/supervisord_0.7.3_Linux_64-bit.tar.gz"
			downloadDir, err = ioutil.TempDir("", "downloadDir")
			if err != nil {
				return packit.BuildResult{}, err
			}
			defer os.RemoveAll(downloadDir)

			err = exec.Command("curl",
				"-L", uri,
				"-o", filepath.Join(downloadDir, "supervisor.tar.gz"),
			).Run()
			if err != nil {
				return packit.BuildResult{}, err
			}

			cmd = exec.Command(
				"tar",
				"-zxf",
				filepath.Join(downloadDir, "supervisor.tar.gz"),
				"-C", nginxLayer.Path,
				"--strip", "1",
			)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			if err = cmd.Run(); err != nil {
				return packit.BuildResult{}, err
			}

		}
		nginxLayer.Cache = true
		nginxLayer.Launch = true

		yamlFile, err := ioutil.ReadFile(filepath.Join(context.WorkingDir, ".platform.app.yaml"))
		if err != nil {
			return packit.BuildResult{}, err
		}
		var config PlatformConfig

		err = yaml.Unmarshal(yamlFile, &config)
		if err != nil {
			return packit.BuildResult{}, err
		}

		nginxConf := filepath.Join(nginxLayer.Path, "conf", "nginx.conf")
		cfg := NginxConfig{
			AppRoot:      context.WorkingDir,
			WebDirectory: config.Web.Locations["/"].Root,
			FpmSocket:    filepath.Join(os.Getenv("PHP_FPM_DIR"), "php-fpm.socket"),
		}
		err = ProcessTemplateToFile(NginxConfTemplate, nginxConf, cfg)
		if err != nil {
			return packit.BuildResult{}, err
		}
		phpFpmCmd := fmt.Sprintf("%s/sbin/php-fpm -c %s -y %s/php-fpm.conf", os.Getenv("PHP_HOME"), os.Getenv("PHP_FPM_DIR"), os.Getenv("PHP_FPM_DIR"))

		nginxCmd := fmt.Sprintf("%s/sbin/nginx -p %s -c %s/conf/nginx.conf", nginxLayer.Path, nginxLayer.Path, nginxLayer.Path)

		supervisorCfg := SupervisorConfig{
			FpmCmd:   phpFpmCmd,
			NginxCmd: nginxCmd,
		}
		supervisorConf := filepath.Join(nginxLayer.Path, "supervisor.conf")
		err = ProcessTemplateToFile(SupervisorConfTemplate, supervisorConf, supervisorCfg)
		if err != nil {
			return packit.BuildResult{}, err
		}

		nginxLayer.SharedEnv.Append("PATH", filepath.Join(os.Getenv("PHP_HOME"), "bin"), string(os.PathListSeparator))
		nginxLayer.SharedEnv.Append("PATH", filepath.Join(os.Getenv("PHP_HOME"), "sbin"), string(os.PathListSeparator))
		nginxLayer.SharedEnv.Append("PATH", nginxLayer.Path, string(os.PathListSeparator))

		return packit.BuildResult{
			Plan: context.Plan,
			Layers: []packit.Layer{
				nginxLayer,
			},
			Launch: packit.LaunchMetadata{
				Processes: []packit.Process{
					{
						Type:    "web",
						Command: fmt.Sprintf("supervisord -c %s", supervisorConf),
					},
				},
			},
		}, nil
	}
}
